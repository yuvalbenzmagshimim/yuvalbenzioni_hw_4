#include "sqlite3.h"
#include <string>
#include<iostream>
using namespace std;
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		cout << azCol[i] << " = " << argv[i] << endl;
	}
	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	// connection to the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	rc = sqlite3_exec(db, "CREATE TABLE People(id INTEGER PRIMARY KEY autoincrement,name TEXT);", callback, 0, &zErrMsg);
	if (rc)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	rc = sqlite3_exec(db, "INSERT INTO People(name) values(\"Yuval\")", callback, 0, &zErrMsg);
	if (rc)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	rc = sqlite3_exec(db, "INSERT INTO People(name) values(\"Another one\")", callback, 0, &zErrMsg);
	if (rc)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	rc = sqlite3_exec(db, "INSERT INTO People(name) values(\"Second another one\")", callback, 0, &zErrMsg);
	if (rc)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	rc = sqlite3_exec(db, "UPDATE People SET name = \"good\" WHERE id = last_insert_rowid()", callback, 0, &zErrMsg);
	if (rc)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	cout << "done!\n";
	system("pause");


}
