#include <string>
#include "sqlite3.h"
#include <vector>
#include <iostream>
#include<map>
using namespace std;
map<string, vector<string>> res;
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = res.find(azCol[i]);
		if (it != res.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			res.insert(p);
		}
	}

	return 0;
}

double getMoney(int id, sqlite3* db, char* zErrMsg,string from_where)
{
	int rc;
	string query = "SELECT * FROM " + from_where + " WHERE id = "+to_string(id);
	res.clear();
	rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		return false;
	}

	return stod(res.find(from_where=="cars"?"price":"balance")->second[0]);
}

bool isAvailable(int id, sqlite3* db, char* zErrMsg)
{
	int rc;
	string query = "SELECT * FROM cars WHERE id = " + to_string(id);
	res.clear();
	rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		return false;
	}
	return stoi(res.find("available")->second[0]) != 0;
}

void tryBuy(sqlite3* db, char* zErr, int manID, int carID)
{
	if (!isAvailable(carID, db, zErr))
	{
		cout << "Car is not available" << endl;
		return;
	}
	double carprice = getMoney(carID, db, zErr, "cars");
	if (getMoney(manID, db, zErr, "accounts") <carprice )
	{
		cout << "Cannot sell, car is too expensive" << endl;
		return;
	}
	int rc;
	string query;
	query = "BEGIN TRANSACTION; UPDATE  accounts SET balance = balance - "+ to_string(carprice) + " WHERE buyer_id = "+ to_string(manID)+"; UPDATE cars SET available = 0  WHERE id = "+ to_string(carID)+"; COMMIT;";
	res.clear();
	rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErr);
	cout << "car successfully sold!\n";
	return;
}

void balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	string query;

	if (getMoney(from,db,zErrMsg,"accounts") >= amount)
	{
		query = "BEGIN TRANSACTION; UPDATE  accounts SET balance = balance - "+ to_string(amount)+ " WHERE buyer_id = "+to_string(from)+"; UPDATE accounts SET balance = balance + "+to_string(amount)+" WHERE buyer_id = "+to_string(to)+"; COMMIT;";

		res.clear();
		rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			return;
		}
		cout << "OK!";
	}
	else
	{
		cout << "Not committed, coustomer too poor";
	}
}

int  main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	tryBuy(db, zErrMsg, 2,1);
	tryBuy(db, zErrMsg, 5, 6);
	tryBuy(db, zErrMsg, 1, 21);
	system("pause");
}
